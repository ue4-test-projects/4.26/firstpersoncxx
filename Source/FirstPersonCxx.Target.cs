using UnrealBuildTool;
using System.Collections.Generic;

public class FirstPersonCxxTarget : TargetRules
{
	public FirstPersonCxxTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.Add("FirstPersonCxx");
	}
}
