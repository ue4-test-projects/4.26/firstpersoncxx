#include "FirstPersonCxxGameMode.h"
#include "FirstPersonCxxHUD.h"
#include "FirstPersonCxxCharacter.h"
#include "UObject/ConstructorHelpers.h"

AFirstPersonCxxGameMode::AFirstPersonCxxGameMode() : Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;
	
	// use our custom HUD class
	HUDClass = AFirstPersonCxxHUD::StaticClass();
}
